# versioney

<img src="static/img/vy.png" style="image-rendering: crisp-edges; float: right; margin: 10px" width="100px" />

versioney aims to be a tiny static site generator written in python.

The distinctive feature of versioney is the way it manages content updates: if enabled,
versioney can track the changes of a file, and provide a way to emphasize this content through styling.

In other words, if you keep having loose files that are topic-centered and you update these pages periodically, 
versioney may be a nice fit. 

**But not now**.

 Now it's just a classic broken static site generator. 

## Installation

```bash
$> #versioney is available on pip
$> pip install --user versioney
$> #creating with -p flag gets a preset
$> #from git
$> vy new -p default website 
$> #edit this to fit your needs
$> vi website/vy.toml
$> #build the site
$> vy build
$> # launch a web server and head to 
$> # localhost:8000
$> python -m http.server

```

Head to [vyfile](/getting-started/vyfile.html) to know more.
