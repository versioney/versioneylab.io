---
title: vyfile "reference" 
---

# vyfile "reference"

Rather than being a reference, this page lists things you can put into your vyfile.
To do so, let's dessicate one here.

### `title, baseurl`

Put the title of your site, along with the baseurl it uses.
For example, use:

```toml
title = "My Gardening Blog"
baseurl = "http://gardening.blog" 
```

### `build`

Specify where your sources are located and where should versioney put the generated site.

```toml
[build]
    source_path = "src/"
    build_path = "public/"
```
_Note: If you use GitLab Pages, it seems that you need that generated folder name has to be `public`_

### `content`

The `content` field describes your site structure, along with the options relevant to each content type.
For now, the two options available are `index` and `order`:

- `index (bool)` tells versioney to build an `index.html` page that will list every entry in this content type.
  Indexing is used when dealing with blog posts, and disabled when being in a directory that hosts pages.

- `order="asc"` tells versioney how it has to order entries. Possible values are "asc", "desc" or ~~"git"~~ (soontm).

  Ordering is done depending on the `date` field of the yaml frontmatter of each entry. Format is YYYY-mm-dd.
  If one of your entries does not have any date field, ordering is not done at all. 

```toml
[content]
    [content.posts]
        index = true 
        order = "asc" 
    [content.getting-started]
        index = true
        order = "asc"
    [content.topics]
        index = true
        order = "asc"
```

### `templates`

This one is _a bit_ more complicated.

Templates are blueprints for each category.
Templates are stored into `source_path/templates`.

When looking for a template to use for an entry, versioney checks, in that order:

- The **YAML frontmatter**, looking for a `template` key, and if not,
- The **`templates` list from the vyfile**, looking for a key corresponding to content type 
  (ex. `post = "my_post_template.mustache"` for the post content type), and if not,
- The **`default` template**, as specified in the yfile.

One last thing: for content type that is ought to be indexed, you can also specify a `content_type_index.mustache`. 
Otherwise, the `default_index.mustache` template will be used.

The whole vyfile is exposed to mustache templates, and is available under `{{vyfile.foo}}`. You can also access YAML Frontmatter stuff under no namespace in particular (though it should be), ex. `{{title}}`.

```toml
[templates]
    default = "default.mustache"
    default_index = "default_index.mustache"
    topics_index = "topics_index.mustache"
    posts_index = "posts_index.mustache"
```

### `markdown2`

For now, it has no use other than being able to add/remove markdown2 extras conviniently. Sorry.
```toml
[markdown2]
    # extras for markdown2. 
    extras = ["fenced-code-blocks", 
        "tables", 
        "task_list", 
        "metadata"]
```