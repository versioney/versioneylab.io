# versioney's git mode

versioney's distinctive feature is a git ordering/displaying style.

As I used and published things on tiny personal websites, I've always found out that I'd have some tiny blog posts, and numerous longer entries (usually lists or notes about a particular subject), that I'd update once in a while.

The problem is that changes would be completely invisible to everyone, since the entry itself would be still published in an earlier date.

Moreover, it wasn't possible to visually separate old and new content.

**Since all of my content is versioned using git, why not use the commit information and add markup to distinguish between older and newer content?**


## How to enable git feature?

Either:

- via yaml frontmatter : `git = true`
- via [vyfile](vyfile.html) ordering


Here is an example of some new content that was added later.


Some content that is added on a later commit too.


this is the last **commit** to ensure markdown is still rendered! :) 

- this
- is
- fun!